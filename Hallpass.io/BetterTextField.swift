//
//  BetterTextField.swift
//  Hallpass.io
//
//  Created by Scott Haley on 9/10/16.
//  Copyright © 2016 Hallpass.io. All rights reserved.
//

import UIKit
import SnapKit

protocol BetterTextFieldDelegate {
    func buttonPressed(sender: BetterTextField, buttonNumber: Int, buttonToggled: Bool?)
}

@IBDesignable class BetterTextField: UIView, UITextFieldDelegate {
    
    var delegate: BetterTextFieldDelegate?
    
    lazy var _textfield = UITextField()
    lazy var _inputUnderline = UIView()
    lazy var _inputUnderlineFocused = UIView()
    lazy var _label = UILabel()
    lazy var _labelFocused = UILabel()
    lazy var _button1 = UIButton(type: UIButtonType.Custom)
    lazy var _button2 = UIButton(type: UIButtonType.Custom)
    
    @IBInspectable var secureTextEntry: Bool = false{
        didSet {
            self._textfield.secureTextEntry = self.secureTextEntry
        }
    }
    
    @IBInspectable var borderColor : UIColor = UIColor.blackColor() {
        didSet {
            self._inputUnderline.backgroundColor = self.borderColor
        }
    }
    
    @IBInspectable var borderColorFocused : UIColor = UIColor.blueColor() {
        didSet {
            self._inputUnderlineFocused.backgroundColor = self.borderColorFocused
        }
    }
    
    @IBInspectable var placeholder : String? {
        didSet {
            self._label.text = self.placeholder
            self._labelFocused.text = self.placeholder
        }
    }
    
    @IBInspectable var labelColorFocused : UIColor = UIColor.blueColor() {
        didSet {
            self._labelFocused.textColor = self.labelColorFocused
        }
    }
    
    @IBInspectable var text : String? {
        didSet {
            self._textfield.text = self.text
        }
    }
    
    @IBInspectable var button1 : UIImage? {
        didSet {
            self.button1 = self.button1?.imageWithRenderingMode(.AlwaysTemplate)
            self._button1.setImage(self.button1, forState: .Normal)
        }
    }
    
    @IBInspectable var button2: UIImage? {
        didSet {
            self.button2 = self.button2?.imageWithRenderingMode(.AlwaysTemplate)
            self._button2.setImage(self.button2, forState: .Normal)
        }
    }
    
    @IBInspectable var buttonColorNormal : UIColor = UIColor.blackColor() {
        didSet {
            self._button1.tintColor = self.buttonColorNormal
            self._button2.tintColor = self.buttonColorNormal
        }
    }
    
    @IBInspectable var buttonColorSelected : UIColor?
    
    @IBInspectable var shouldButton1Toggle : Bool = false
    @IBInspectable var button1Toggled : Bool = false {
        didSet {
            if self.shouldButton1Toggle && self.buttonColorSelected != nil {
                self._button1.tintColor = self.button1Toggled ? self.buttonColorSelected : self.buttonColorNormal
            }
        }
    }
    
    @IBInspectable var shouldButton2Toggle : Bool = false
    @IBInspectable var button2Toggled : Bool = false {
        didSet {
            if self.shouldButton2Toggle && self.buttonColorSelected != nil {
                self._button2.tintColor = self.button2Toggled ? self.buttonColorSelected! : self.buttonColorNormal
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func intrinsicContentSize() -> CGSize {
        return CGSize(width: 240, height: 65)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        self._textfield.delegate = self
        
        self._textfield.font = UIFont(name: "Roboto", size: 20)
        self._textfield.clearButtonMode = .WhileEditing
        self._textfield.autocapitalizationType = .None
        self._textfield.autocorrectionType = .No
        self._textfield.addTarget(self, action: #selector(textFieldEditingBegin), forControlEvents: .EditingDidBegin)
        self._textfield.addTarget(self, action: #selector(textFieldEditingEnded), forControlEvents: .EditingDidEnd)
        self.addSubview(self._textfield)
        self._textfield.snp_makeConstraints { make in
            make.width.equalTo(self).offset(-5)
            make.centerX.equalTo(self)
            make.top.equalTo(self)
            make.bottom.equalTo(self).offset(-32)
        }
        
        self.addSubview(self._inputUnderline)
        self._inputUnderline.snp_makeConstraints { make in
            make.width.equalTo(self)
            make.centerX.equalTo(self)
            make.bottom.equalTo(self).offset(-31)
            make.height.equalTo(2)
        }
        
        self._inputUnderline.addSubview(self._inputUnderlineFocused)
        self._inputUnderlineFocused.snp_makeConstraints { make in
            make.width.equalTo(0)
            make.top.left.bottom.equalTo(self._inputUnderline)
        }
        
        self._label.font = UIFont(name: "Roboto", size: 13)
        self.addSubview(self._label)
        self._label.snp_makeConstraints { make in
            make.width.equalTo(self).offset(-5)
            make.centerX.equalTo(self)
            make.bottom.equalTo(self).offset(-10)
        }
        
        self._labelFocused.font = UIFont(name: "Roboto", size: 13)
        self._labelFocused.alpha = 0
        self.addSubview(self._labelFocused)
        self._labelFocused.snp_makeConstraints { make in
            make.width.equalTo(self).offset(-5)
            make.centerX.equalTo(self)
            make.bottom.equalTo(self).offset(-10)
        }
        
        if (self.button1 != nil) {
            self._button1.addTarget(self, action: #selector(button1Touched), forControlEvents: .TouchUpInside)
            self.addSubview(self._button1)
            self._button1.snp_makeConstraints { make in
                make.width.height.equalTo(25)
                make.bottom.right.equalTo(self)
            }
        }
        
        if (self.button2 != nil) {
            self._button2.addTarget(self, action: #selector(button2Touched), forControlEvents: .TouchUpInside)
            self.addSubview(self._button2)
            self._button2.snp_makeConstraints { make in
                make.width.height.equalTo(25)
                make.bottom.equalTo(self)
                make.right.equalTo(self).offset(-30)
            }
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self._textfield.resignFirstResponder()
        return true
    }
    
    func textFieldEditingBegin() {
        UIView.animateWithDuration(0.3) {
            self._inputUnderlineFocused.snp_updateConstraints { make in
                make.width.equalTo(self._inputUnderline.frame.width)
            }
            self._label.alpha = 0
            self._labelFocused.alpha = 1
            self._inputUnderline.layoutIfNeeded()
        }
    }
    
    func textFieldEditingEnded() {
        UIView.animateWithDuration(0.3) {
            self._inputUnderlineFocused.snp_updateConstraints { make in
                make.width.equalTo(0)
            }
            self._label.alpha = 1
            self._labelFocused.alpha = 0
            self._inputUnderline.layoutIfNeeded()
        }
    }
    
    func button1Touched() {
        if (self.shouldButton1Toggle) {
            self.button1Toggled = !self.button1Toggled
        } else {
            self._button1.tintColor = self.buttonColorSelected
            UIView.transitionWithView(self._button1, duration: 0.1, options: [.TransitionCrossDissolve], animations: nil, completion: { success in
                self._button1.tintColor = self.buttonColorNormal
                UIView.transitionWithView(self._button1, duration: 2.0, options: [.TransitionCrossDissolve], animations: nil, completion: nil)
            })
        }
            
        if let delegate = self.delegate {
            delegate.buttonPressed(self, buttonNumber: 1, buttonToggled: self.button1Toggled)
        }
    }
    
    func button2Touched() {
        if (self.shouldButton2Toggle) {
            self.button2Toggled = !self.button2Toggled
        }
        
        if let delegate = self.delegate {
            delegate.buttonPressed(self, buttonNumber: 2, buttonToggled: self.button2Toggled)
        }
    }
}
