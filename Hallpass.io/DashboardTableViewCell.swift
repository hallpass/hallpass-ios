//
//  CredentialTableViewCell.swift
//  Hallpass.io
//
//  Created by Scott Haley on 6/8/16.
//  Copyright © 2016 Hallpass.io. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class DashboardTableViewCell: MGSwipeTableCell {

    // MARK: Properties
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var circleIdentifier: CircleIdentifier!
    
    internal var ID: String? = nil
    var refreshCallback: (() -> Void)? = nil
    var dashboardView: DashboardController? = nil
    
    func clear() {
//        self.circleIdentifier.circleL
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let deleteButton = MGSwipeButton(title: "", icon: UIImage(named: "ic_delete"), backgroundColor: UIColor.whiteColor()) { (sender: MGSwipeTableCell!) -> Bool in
            
            if (self.ID != nil) {
                API.sharedInstance.emitWithDevice("deletesites", data: ["sites": [self.ID!]])
                API.sharedInstance.on("deletesites") { data in
                    if (data[0] as! Bool) {
                        self.refreshCallback?()
                    } else {
                        let alertController = UIAlertController(title: "Destructive", message: "Simple alertView demo with Destructive and Ok.", preferredStyle: UIAlertControllerStyle.Alert)
                        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (result : UIAlertAction) -> Void in
                                    print("OK")
                                }
                        alertController.addAction(okAction)
                        self.dashboardView?.presentViewController(alertController, animated: true, completion: nil)
                    }
                }
            }
            return true;
        }
        self.rightButtons = [deleteButton]
    }
    
    func onRefresh(view: DashboardController, callback: () -> Void) {
        self.refreshCallback = callback
        self.dashboardView = view
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
