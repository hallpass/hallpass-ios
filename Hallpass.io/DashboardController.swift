//
//  DashboardController.swift
//  Hallpass.io
//
//  Created by Scott Haley on 5/9/16.
//  Copyright © 2016 Hallpass.io. All rights reserved.
//

import UIKit
import SwiftyJSON

class DashboardController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UITabBarDelegate  {

    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tabBar: UITabBar!
    
    var sites = [DashboardRow]()
    var clients = [DashboardRow]()
    var devices = [DashboardRow]()
    var tableList = [DashboardRow]()
    var filtered:[DashboardRow] = []
    var searchActive = false
    
    var selectedTab = 0
    
    var eventsRegistered = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.searchBar.delegate = self
        
        self.tabBar.delegate = self
        
        self.searchBar.returnKeyType = .Done
        self.searchBar.enablesReturnKeyAutomatically = false
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        
        self.tabBar.selectedItem = self.tabBar.items![0] 
        
        let userInfo = API.deviceData()!
        
        API.sharedInstance.emit("getcredlist", data: userInfo)
        API.sharedInstance.emit("getclientlist", data: userInfo)
        API.sharedInstance.emitWithDevice("getdevicelist", data: [:])
        
        if (!eventsRegistered) {
            API.sharedInstance.on("getcredlist", single: false) {data in
                let json = JSON(data[0])
                self.sites.removeAll()
                for (key, subJson) in json {
//                    let color = UIColor(hexString: subJson["color"].stringValue)
//                    let s = CredentialSite(url: subJson["site"].stringValue, name: subJson["title"].stringValue, color: color, key: key, initial: subJson["initial"].stringValue)
                    let s = DashboardRow(title: subJson["title"].stringValue, color: subJson["color"].stringValue, initial: subJson["initial"].stringValue, id: key)
                    self.sites.append(s!)
                }
                self.tableList = self.sites
                self.tableView.reloadData()
            }
            
            API.sharedInstance.on("getclientlist", single: false) {data in
                let json = JSON(data[0])
                
                self.clients.removeAll()
                for (key, subJson) in json {
                    let c = DashboardRow(title: subJson["title"].stringValue, color: subJson["color"].stringValue, initial: subJson["initial"].stringValue, id: key)
                    self.clients.append(c!)
                }
            }
            
            API.sharedInstance.on("getdevicelist", single: false) {data in
                let json = JSON(data[0])
                
                self.devices.removeAll()
                for (key, subJson) in json {
                    let d = DashboardRow(title: subJson["title"].stringValue, color: subJson["color"].stringValue, initial: subJson["initial"].stringValue, id: key)
                    self.devices.append(d!)
                }
            }
            
            API.sharedInstance.on("reason", single: false) {data in
                let json = JSON(data[0])
                
                print("Reason: ", json)
                
                var title = ""
                
                if json["site"].string != nil {
                    title = json["site"].string!
                }
                if json["client"].string != nil {
                    title = json["client"].string!
                }
                let description = json["description"].string
                let account = json["accounts"][0].string
                let clientVerified = json["clientVerified"].bool
                
                API.sharedInstance.requestAuth(self, title: title, description: description!, account: account, clientVerified: clientVerified) { success in
                    
                }
            }
            
            eventsRegistered = true
        }
    }
    
    func registerEvents() {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        print("began editting")
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        self.searchActive = false;
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        self.searchActive = false;
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.searchActive = false;
        self.searchBar.resignFirstResponder()
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.characters.count == 0 {
            self.searchActive = false
        } else {
            self.searchActive = true
            self.filtered = self.tableList.filter({ (row) -> Bool in
                let tmp: NSString = row.searchString()
                let range = tmp.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch)
                return range.location != NSNotFound
            })
        }


        self.tableView.reloadData()
    }
    
    // number of rows in table view
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.searchActive {
            return self.filtered.count
        }
        return self.tableList.count
    }
    
    // create a cell for each table view row
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var dashboardRow: DashboardRow
        if self.searchActive {
            dashboardRow = self.filtered[indexPath.row]
        } else {
            dashboardRow = self.tableList[indexPath.row]
        }
        
        let cell = self.tableView.dequeueReusableCellWithIdentifier("DashboardRowCell") as! DashboardTableViewCell!
        
        cell.title.text = dashboardRow.Title
        cell.circleIdentifier.circleLabelText(dashboardRow.Initial)
        cell.circleIdentifier.circleColor = dashboardRow.Color
        cell.ID = dashboardRow.ID
        
        switch (self.selectedTab) {
        case 1:
            
            break
        case 2:
            
            break
        default:
            cell.onRefresh(self) {
                API.sharedInstance.emitWithDevice("getcredlist", data: [:])
            }
            break
        }
        
        let bgColorView = UIView()
//        bgColorView.backgroundColor = UIColor(colorLiteralRed: 150/255, green: 208/255, blue: 227/255, alpha: 1)
        bgColorView.backgroundColor = dashboardRow.Color
        cell.selectedBackgroundView = bgColorView
        
//        cell.selectionStyle = .None
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var dashboardRow: DashboardRow
        if self.searchActive {
            dashboardRow = self.filtered[indexPath.row]
        } else {
            dashboardRow = self.tableList[indexPath.row]
        }
        let detailsPage = storyboard?.instantiateViewControllerWithIdentifier("Details") as? DetailsController
        detailsPage?.fromRow(dashboardRow)
        self.presentViewControllerFromVisibleViewController(detailsPage!, animated: true, completion: {})
    }
    
    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        self.selectedTab = item.tag
        switch (self.selectedTab) {
        case 1:
            self.tableList = self.devices
            break
        case 2:
            self.tableList = self.clients
            break
        default:
            self.tableList = self.sites
            break
        }
        self.tableView.reloadData()
    }
}
