//
//  CreatePinController.swift
//  Hallpass.io
//
//  Created by Scott Haley on 5/9/16.
//  Copyright © 2016 Hallpass.io. All rights reserved.
//

import UIKit
import MapKit

class PinController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var pinPad: UIView!
    @IBOutlet weak var pinPadContainer: UIView!
    @IBOutlet weak var newComputerContainer: UIView!
    @IBOutlet weak var publicPersonalView: UIView!
    @IBOutlet weak var personalComputerView: UIView!
    @IBOutlet weak var pinTitleLabel: UILabel!
    @IBOutlet weak var pinReasonLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var publicButton: UIButton!
    @IBOutlet weak var personalButton: UIButton!
    @IBOutlet weak var personalName: UITextField!
    
    @IBOutlet weak var pinCircleStack: UIStackView!
   
    var pinCircles: [CAShapeLayer] = []
    var typedPin: String = ""
    
    enum PinType {
        case Create
        case Normal
    }
    
    var pinType: PinType = .Normal
    var pinCreateStep: Int = 0
    var pinCreateFirst: String = ""
    var clientName : String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (API.sharedInstance.clientVerified) {
            self.newComputerContainer.alpha = 0
        }
        
        self.pinType = API.sharedInstance.pinPadType
        
        self.pinTitleLabel.text = API.sharedInstance.pinPadTitle
        if self.pinType == .Normal {
            self.pinReasonLabel.text = API.sharedInstance.pinPadDescription
        } else {
            self.pinReasonLabel.text = ""
            self.cancelButton.hidden = true
        }
        
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: 20, y:20), radius: 10, startAngle: 0, endAngle: CGFloat(M_PI * 2), clockwise: true)
        
        
        for circleView in self.pinCircleStack.subviews as [UIView] {
            let shapeLayer = CAShapeLayer()
            shapeLayer.path = circlePath.CGPath
            shapeLayer.fillColor = UIColor.clearColor().CGColor
            shapeLayer.strokeColor = UIColor.whiteColor().CGColor
            shapeLayer.lineWidth = 1.0
            self.pinCircles.append(shapeLayer)
            
            circleView.layer.addSublayer(shapeLayer)
        }
        
        publicButton.backgroundColor = UIColor.clearColor()
        publicButton.layer.cornerRadius = 20
        publicButton.layer.borderWidth = 1
        publicButton.layer.borderColor = UIColor.whiteColor().CGColor
        
        personalButton.backgroundColor = UIColor.clearColor()
        personalButton.layer.cornerRadius = 20
        personalButton.layer.borderWidth = 1
        personalButton.layer.borderColor = UIColor.whiteColor().CGColor
        
        self.mapView.delegate = self;
    }
    
    override func viewDidLayoutSubviews() {
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.whiteColor().CGColor
        border.frame = CGRect(x: 0, y: self.personalName.frame.size.height - width, width:  self.personalName.frame.size.width, height: width)

        border.borderWidth = width
        self.personalName.layer.addSublayer(border)
        self.personalName.layer.masksToBounds = true
    }
    
    func loadLocation(lat: Double, lon: Double, city: String, state: String, country: String) {
        let location = CLLocationCoordinate2D(
          latitude: lat,
          longitude: lon
        )
        // 2
        let span = MKCoordinateSpanMake(0.1, 0.1)
        let region = MKCoordinateRegion(center: location, span: span) 
        mapView.setRegion(region, animated: true)
        
//        let annotation = MKPointAnnotation()
//        annotation.coordinate = location
//        annotation.title = city + ", " + state;
//        annotation.subtitle = country;
//        mapView.addAnnotation(annotation)
        
        mapView.addOverlay(MKCircle(centerCoordinate: location, radius: 5000))
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        let circleRenderer = MKCircleRenderer(overlay: overlay)
        circleRenderer.fillColor = UIColor.blueColor().colorWithAlphaComponent(0.1)
        circleRenderer.strokeColor = UIColor.blueColor()
        circleRenderer.lineWidth = 1
        return circleRenderer
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        for sview in self.pinPad.subviews as [UIView] {
            if let stack = sview as? UIStackView {
                for sview2 in stack.subviews as [UIView] {
                    if let button = sview2 as? UIButton {
                        button.backgroundColor = UIColor.clearColor()
                        button.layer.cornerRadius = 0.5 * button.bounds.size.width
                        button.layer.borderWidth = 1
                        button.layer.borderColor = UIColor.whiteColor().CGColor
                        button.addTarget(self, action: #selector(PinController.pinButtonTouched(_:)), forControlEvents: .TouchUpInside)
                    }
                }
            } else {
                if let button = sview as? UIButton {
                    button.backgroundColor = UIColor.clearColor()
                    button.layer.cornerRadius = 0.5 * button.bounds.size.width
                    button.layer.borderWidth = 1
                    button.layer.borderColor = UIColor.whiteColor().CGColor
                    button.addTarget(self, action: #selector(PinController.pinButtonTouched(_:)), forControlEvents: .TouchUpInside)
                }
            }
        }
        
        if (API.sharedInstance.clientVerified) {
            self.showPinContainer()
        }
    }
    
    func showPinContainer() {
        UIView.animateWithDuration(0.5, animations: {
            self.newComputerContainer.alpha = 0
            self.pinPadContainer.alpha = 1
            self.pinCircleStack.alpha = 1
        })
    }
    
    func showPersonalComputerView() {
        UIView.animateWithDuration(0.5, animations: {
            self.publicPersonalView.alpha = 0
            self.personalComputerView.alpha = 1
            self.personalName.becomeFirstResponder()
        })
    }
    
    func pinButtonTouched(sender: UIButton!) {
        self.typedPin += sender.titleLabel!.text!
        if self.typedPin.characters.count == 4 {
            if self.pinType == .Create {
                if self.pinCreateStep == 0 {
                    self.pinCreateFirst = self.typedPin
                    self.pinCreateStep = 1
                    self.pinReasonLabel.text = "Confirm Pin"
                } else if self.pinCreateStep == 1 {
                    if self.typedPin == self.pinCreateFirst {
                        API.sharedInstance.submitPin(self.typedPin, clientName: self.clientName).then { success -> Void in
                            self.dismissPinPad(success)
                        }
                    } else {
                        self.pinReasonLabel.text = "Pins Did Not Match"
                        self.pinCreateStep = 0
                    }
                }
                let dispatchTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.25 * Double(NSEC_PER_SEC)))
                dispatch_after(dispatchTime, dispatch_get_main_queue(), {
                    self.typedPin = ""
                    self.refreshPinCircles()
                })
            } else {
                API.sharedInstance.submitPin(self.typedPin, clientName: self.clientName).then { success -> Void in
                    if success {
                        self.dismissPinPad(true)
                    } else {
                        self.pinReasonLabel.text = "Incorrect Pin"
                        self.typedPin = ""
                        self.refreshPinCircles()
                    }
                }
            }
        }
        self.refreshPinCircles()
        
    }
    
    func refreshPinCircles() {
        for index in 0...3 {
            self.pinCircles[index].fillColor = UIColor.clearColor().CGColor
        }
        if self.typedPin.characters.count != 0 {
            for index in 0...(self.typedPin.characters.count - 1) {
                self.pinCircles[index].fillColor = UIColor.whiteColor().CGColor
            }
        }
    }
    
    @IBAction func deleteButtonTouched(sender: UIButton) {
        if self.typedPin.characters.count == 0 {
            return
        }
        self.typedPin.removeAtIndex(self.typedPin.endIndex.predecessor())
        self.refreshPinCircles()
    }
    
    @IBAction func cancelButtonTouched(sender: UIButton) {
        if self.pinType == .Create {
            return
        }
        API.sharedInstance.submitPin("null")
        self.dismissPinPad(false)
    }
    
    @IBAction func personalSaveButtonTouched(sender: AnyObject) {
        self.clientName = self.personalName.text
        self.showPinContainer()
    }
    
    @IBAction func personalCancelButtonTouched(sender: AnyObject) {
        API.sharedInstance.submitPin("null")
        self.dismissPinPad(false)
    }
    
    @IBAction func publicButtonTouched(sender: UIButton) {
        self.showPinContainer()
    }
    
    @IBAction func personalButtonTouched(sender: UIButton) {
        self.showPersonalComputerView()
    }
    
    func dismissPinPad(success: Bool) {
        self.dismissViewControllerAnimated(true, completion: nil)
        API.sharedInstance.pinPadFinished(success)
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func viewDidDisappear(animated: Bool) {
        self.mapView.delegate = nil
        self.mapView.showsUserLocation = false;
        self.mapView.removeFromSuperview()
        self.mapView = nil
    }
   

}
