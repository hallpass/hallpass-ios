//
//  DetailsController.swift
//  Hallpass.io
//
//  Created by Scott Haley on 9/5/16.
//  Copyright © 2016 Hallpass.io. All rights reserved.
//

import UIKit

class DetailsController: UIViewController, BetterTextFieldDelegate {

    @IBOutlet weak var TopBar: UIView!
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var BackButton: UIButton!
    @IBOutlet weak var AccountButton: UIButton!
    @IBOutlet weak var UsernameField: BetterTextField!
    @IBOutlet weak var PasswordField: BetterTextField!
    
    var RowData: DashboardRow? = nil
    
    func fromRow(row: DashboardRow) {
        self.RowData = row
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.UsernameField.delegate = self
        self.PasswordField.delegate = self

        if (self.RowData != nil) {
            self.TopBar.backgroundColor = self.RowData!.Color
            
            self.TitleLabel.text = self.RowData!.Title
            
            self.BackButton.layer.cornerRadius = 20
            self.BackButton.layer.borderWidth = 1
            self.BackButton.layer.borderColor = self.RowData!.Color.CGColor
            self.BackButton.setTitleColor(self.RowData!.Color, forState: .Normal)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonTouched(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: {})
    }
    
    func buttonPressed(sender: BetterTextField, buttonNumber: Int, buttonToggled: Bool?) {
        if sender == self.PasswordField {
            if buttonNumber == 2 {
                self.PasswordField.secureTextEntry = !buttonToggled!
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
