//
//  AddClientController.swift
//  Hallpass.io
//
//  Created by Scott Haley on 7/15/16.
//  Copyright © 2016 Hallpass.io. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddClientController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        
        API.sharedInstance.emitWithDevice("client", data: ["accept": true])
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        
        API.sharedInstance.emitWithDevice("client", data: ["accept": false])
    }

    @IBAction func addClientFinished(sender: UIButton) {
        navigationController?.popViewControllerAnimated(true)
    }
}
