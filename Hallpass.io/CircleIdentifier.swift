//
//  CircleIndicator.swift
//  Hallpass.io
//
//  Created by Scott Haley on 6/7/16.
//  Copyright © 2016 Hallpass.io. All rights reserved.
//

import UIKit

@IBDesignable
class CircleIdentifier: UIView {
    
    var CircleLabel: UILabel? = nil
    var circleLabelText: String?
    
    @IBInspectable var circleColor: UIColor? {
        didSet {
            self.layer.backgroundColor = circleColor?.CGColor
        }
    }
    
    @IBInspectable var Label: String? {
        didSet {
            self.circleLabelText = Label
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.cornerRadius = 0.5 * self.frame.width
        self.layer.masksToBounds = true
        if (self.CircleLabel == nil) {
            self.CircleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
            self.CircleLabel?.text = self.circleLabelText
            self.CircleLabel?.textAlignment = .Center
            self.CircleLabel?.textColor = UIColor.whiteColor()
            self.addSubview(self.CircleLabel!)
        }
    }
    
    func circleLabelText(label: String) {
        self.circleLabelText = label
        self.CircleLabel?.text = self.circleLabelText
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
