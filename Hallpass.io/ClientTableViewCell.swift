//
//  ClientTableViewCell.swift
//  Hallpass.io
//
//  Created by Scott Haley on 7/18/16.
//  Copyright © 2016 Hallpass.io. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class ClientTableViewCell: MGSwipeTableCell {
    
    @IBOutlet weak var clientName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let deleteButton = MGSwipeButton(title: "Delete", backgroundColor: UIColor.redColor())
        self.rightButtons = [deleteButton]
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}