//
//  api.swift
//  Hallpass.io
//
//  Created by Scott Haley on 5/9/16.
//  Copyright © 2016 Hallpass.io. All rights reserved.
//

import Foundation
import SocketIOClientSwift
import PromiseKit
import SwiftyJSON

class API {
//    static let HOST = "http://hallpass.io:8088"
    static let HOST = "http://104.131.185.124:8088"
//    static let HOST = "http://localhost:8088"
    // static let HOST = "http://192.168.1.64:8088"
    static let sharedInstance = API()
    var socket: SocketIOClient

    var pinPadOpened: Bool = false
    var pinPadCallback: ((success: Bool) -> Void)?
    var pinPadTitle: String = ""
    var pinPadDescription: String = ""
    var clientVerified: Bool = false
    var authAccount: String? = nil
    var pinPadType: PinController.PinType = PinController.PinType.Normal
    var cPinPad : PinController? = nil
    
    var eventListeners: [String : [([AnyObject]) -> Void]] = [:]
    
    init() {
        self.socket = SocketIOClient(socketURL: NSURL(string: API.HOST)!)
    }
    
    static func deviceData() -> [String: AnyObject]? {
        let defaults = NSUserDefaults.init(suiteName: "group.hallpass.io")
        if let suid = defaults?.valueForKey("suid") as? String {
            let deviceToken = defaults?.valueForKey("deviceToken")
            let deviceName = defaults?.valueForKey("deviceName")
            return ["suid": suid, "deviceToken": deviceToken as! String, "deviceName": deviceName as! String]
        }
        return nil
    }
    
    func connect() {
        self.connect(true);
    }
    
    func connect(listeners: Bool) {
        self.socket.connect()
        
        if listeners {
            self.on("connect") { data in
                self.emitWithDevice("suid", data: [:])
            }
            
            self.on("handshake", single: false) {data in
                self.emitWithDevice("handshake", data: [:])
            }
            
            self.on("location", single: false) {data in
                let json = JSON.parse(data[0] as! String)
                
                let location = json["ll"];
                let city = json["city"].stringValue;
                let state = json["region"].stringValue;
                let country = json["country"].stringValue;
                
                self.cPinPad?.loadLocation(location[0].doubleValue, lon: location[1].doubleValue , city: city, state: state, country: country)
            }
        }
    }
    
    func on(event: String, callback: ([AnyObject]) -> Void) {
        self.on(event, single: true, callback: callback)
    }
    
    func on(event: String, single: Bool, callback: ([AnyObject]) -> Void) {
        if single {
            if !self.eventListeners.keys.contains(event) {
                self.eventListeners[event] = []
                self.socket.on(event) { data, ack in
                    self.onCallback(event, data: data, ack: ack)
                }
            }
            eventListeners[event]?.append(callback)
        } else {
            self.socket.on(event) {data, ack in
                callback(data)
            }
        }
    }
    
    func onCallback(event: String, data: [AnyObject], ack: SocketAckEmitter) {
        if self.eventListeners.keys.contains(event) {
            for cb in (self.eventListeners[event])! {
                cb(data)
            }
            self.eventListeners[event] = []
        }
    }
    
    func emit(event: String, data: AnyObject) {
        self.socket.emit(event, data)
    }
    
    func emitWithDevice(event: String, data: [String: AnyObject]) -> Bool {
        if var deviceData = API.deviceData() {
            deviceData.update(data)
            self.socket.emit(event, deviceData)
            return true
        }
        return false
    }
    
    private func presentPinPad(view: UIViewController, title: String, description: String, finished: ((success: Bool) -> Void)?) {
        if self.pinPadOpened {
            return
        }
        self.pinPadTitle = title
        self.pinPadDescription = description
        let storyboard = UIStoryboard(name: "Request", bundle: nil)
        self.cPinPad = storyboard.instantiateViewControllerWithIdentifier("Pin") as? PinController
        view.presentViewControllerFromVisibleViewController(cPinPad!, animated: true, completion: {})
        self.pinPadOpened = true
        self.pinPadCallback = finished
    }
    
    func requestAuth(view: UIViewController, title: String, description: String, finished: ((success: Bool) -> Void)?) {
        self.requestAuth(view, title: title, description: description, account: nil, clientVerified: false, finished: finished)
    }
    
    func requestAuth(view: UIViewController, title: String, description: String, account: String?, finished: ((success: Bool) -> Void)?) {
        self.requestAuth(view, title: title, description: description, account: account, clientVerified: false, finished: finished)
    }
    
    func requestAuth(view: UIViewController, title: String, description: String, account: String?, clientVerified: Bool?, finished: ((success: Bool) -> Void)?) {
        self.pinPadType = PinController.PinType.Normal
        self.authAccount = account
        self.clientVerified = clientVerified ?? false
        self.presentPinPad(view, title: title, description: description, finished: finished)
    }
    
    func createPin(view: UIViewController, finished: ((success: Bool) -> Void)?) {
        self.pinPadType = PinController.PinType.Create
        self.presentPinPad(view, title: "Create Pin", description: "", finished: finished)
    }
    
    func submitPin(pin: String) -> Promise<Bool> {
        return self.submitPin(pin, clientName: nil)
    }
    
    func submitPin(pin: String, clientName: String?) -> Promise<Bool> {
        return Promise { fullfill, reject in
            if pinPadType == PinController.PinType.Create {
                self.on("addpin") {data in
                    fullfill(true)
                }
                self.emitWithDevice("addpin", data: ["pin": pin])
            } else {
                var authData : [String: AnyObject] = ["suid": API.deviceData()!["suid"]! as! String, "pin": pin as String]
                if (clientName != nil) {
                    authData["client"] = ["name": clientName!]
                }
                if (self.authAccount != nil) {
                    authData["account"] = self.authAccount! as String
                }
                print(authData)
                self.on("auth") {data in
                    fullfill(data[0] as! Bool)
                }
                self.emit("auth", data: authData)
            }
        }
    }
    
    func pinPadFinished(success: Bool) {
        self.pinPadOpened = false
        pinPadCallback?(success: success)
        pinPadCallback = nil
        cPinPad = nil
    }
    
    func randomAlphanumericString(length: Int) -> String {
        
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".characters
        let lettersLength = UInt32(letters.count)
        
        let randomCharacters = (0..<length).map { i -> String in
            let offset = Int(arc4random_uniform(lettersLength))
            let c = letters[letters.startIndex.advancedBy(offset)]
            return String(c)
        }
        
        return randomCharacters.joinWithSeparator("")
    }
}

extension Dictionary {
    mutating func update(other:Dictionary) {
        for (key,value) in other {
            self.updateValue(value, forKey:key)
        }
    }
}

extension UIViewController {
    func presentViewControllerFromVisibleViewController(viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)?) {
        if let navigationController = self as? UINavigationController {
            navigationController.topViewController?.presentViewControllerFromVisibleViewController(viewControllerToPresent, animated: flag, completion: completion)
        } else if let tabBarController = self as? UITabBarController {
            tabBarController.selectedViewController?.presentViewControllerFromVisibleViewController(viewControllerToPresent, animated: flag, completion: completion)
        } else if let presentedViewController = presentedViewController {
            presentedViewController.presentViewControllerFromVisibleViewController(viewControllerToPresent, animated: flag, completion: completion)
        } else {
            presentViewController(viewControllerToPresent, animated: flag, completion: completion)
        }
    }
}
