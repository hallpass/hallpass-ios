//
//  DeviceTableViewCell.swift
//  Hallpass.io
//
//  Created by Scott Haley on 8/27/16.
//  Copyright © 2016 Hallpass.io. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class DeviceTableViewCell: MGSwipeTableCell {
    
    @IBOutlet weak var deviceName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let deleteButton = MGSwipeButton(title: "Delete", backgroundColor: UIColor.redColor())
        self.rightButtons = [deleteButton]
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}