//
//  ViewController.swift
//  Hallpass.io
//
//  Created by Scott Haley on 5/8/16.
//  Copyright © 2016 Hallpass.io. All rights reserved.
//

import UIKit
import SocketIOClientSwift

class RegisterController: UIViewController {

    @IBOutlet weak var logoTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var logoWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var registerForm: UIView!
    @IBOutlet weak var checkConnectionLabel: UILabel!
    @IBOutlet weak var loadingCircle: UIActivityIndicatorView!
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var deviceTextField: UITextField!
    
    @IBOutlet weak var joinButton: UIButton!
    @IBOutlet weak var joinButtonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var linkButton: UIButton!
    
    var originalTopConstraint: CGFloat = 0.0
    var linkingDevice: Bool = false
    var initialLoad: Bool = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        
        self.originalTopConstraint = self.logoTopConstraint.constant
        self.logoTopConstraint.constant = screenSize.height / 2 - logo.frame.height
        self.view.layoutIfNeeded()
        
        self.registerForm.alpha = 0
        self.loadingCircle.alpha = 0
        
        API.sharedInstance.connect()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if (initialLoad) {
            let defaults = NSUserDefaults.init(suiteName: "group.hallpass.io")
            if (defaults?.valueForKey("suid") != nil) {
                API.sharedInstance.on("verified", callback: {success in
                    if success[0] as! Bool {
                        self.showDashboard()
                    } else {
                        defaults?.removeObjectForKey("suid")
                        defaults?.removeObjectForKey("deviceToken")
                        defaults?.removeObjectForKey("deviceName")
                        defaults?.synchronize()
                        self.showRegisterForm()
                    }

                })
            } else {
                self.showRegisterForm()
            }
            initialLoad = false
        }
        
        let delay = 4 * Double(NSEC_PER_SEC)
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
        dispatch_after(time, dispatch_get_main_queue()) {
//            self.checkConnectionLabel.hidden = false
        }
    }
    
    func showDashboard() {
        UIView.animateWithDuration(0.5, delay: 0, options: .CurveEaseOut, animations: {
            self.logoTopConstraint.constant = 15
            self.logoWidthConstraint.constant = -63
            
            self.view.layoutIfNeeded()
        }, completion: { finished in
            let dashboardController = self.storyboard?.instantiateViewControllerWithIdentifier("Dashboard")
            dashboardController?.modalTransitionStyle = .CrossDissolve
            self.presentViewController(dashboardController!, animated: true, completion: nil)
        })
    }
    
    func showRegisterForm() {
        UIView.animateWithDuration(0.5, delay: 0.5, options: .CurveEaseOut, animations: {
            self.logoTopConstraint.constant = self.originalTopConstraint
            self.view.layoutIfNeeded()
            self.registerForm.alpha = 1
        }, completion: { finished in
                
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func linkTouched(sender: AnyObject) {
        UIView.animateWithDuration(0.2, animations: {
            if !self.linkingDevice {
                self.linkButton.setTitle("Register New Account", forState: .Normal)
                self.joinButton.setTitle("Link Device", forState: .Normal)
                self.joinButtonWidthConstraint.constant = 150
            } else {
                self.linkButton.setTitle("Link Device", forState: .Normal)
                self.joinButton.setTitle("Join", forState: .Normal)
                self.joinButtonWidthConstraint.constant = 120
            }
            self.view.layoutIfNeeded()
        }, completion: { finished in
            self.linkingDevice = !self.linkingDevice
        })
    }
    
    @IBAction func joinTouched(sender: AnyObject) {
        self.showLoadingCircle()
        if self.linkingDevice {
            self.performLink()
        } else {
            self.performRegistration()
        }
    }
    
    func showLoadingCircle() {
        UIView.animateWithDuration(0.5, animations: {
            self.registerForm.alpha = 0
        }, completion: { finished in
            UIView.animateWithDuration(0.5, animations: {
                self.loadingCircle.alpha = 1
            })
        })
    }
    
    func performLink() {
        let suid = self.usernameTextField.text!
        let deviceName = self.deviceTextField.text!
        API.sharedInstance.on("adddevice") {data in
            self.registrationResponse(data[0], deviceName: deviceName, suid: suid) {
                self.showDashboard()
            }
        }
        
        let registerData: [String: String] = ["suid" : suid, "deviceName" : deviceName]
        API.sharedInstance.emit("adddevice", data: registerData)
    }
    
    func performRegistration() {
        let suid = self.usernameTextField.text!
        let deviceName = self.deviceTextField.text!
        API.sharedInstance.on("register") {data in
            self.registrationResponse(data[0], deviceName: deviceName, suid: suid) {
                API.sharedInstance.createPin(self) {(success) in
                    self.showDashboard()
                }
            }
        }
        
        let registerData: [String: String] = ["suid" : suid, "deviceName" : deviceName]
        API.sharedInstance.emit("register", data: registerData)
    }
    
    func registrationResponse(deviceToken: AnyObject, deviceName: String, suid: String, success: () -> Void) {
        if !(deviceToken is Bool) {
            let defaults = NSUserDefaults.init(suiteName: "group.hallpass.io")
            defaults?.setValue(deviceToken, forKey: "deviceToken")
            defaults?.setValue(deviceName, forKey: "deviceName")
            defaults?.setValue(suid, forKey: "suid")
            defaults?.synchronize()
            
            success()
        } else {
            UIView.animateWithDuration(0.5, animations: {
                self.registerForm.alpha = 1
                self.loadingCircle.alpha = 0
                self.usernameTextField.text = ""
            })
        }
    }
    

}

