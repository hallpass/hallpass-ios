//
//  PinPad.swift
//  Hallpass.io
//
//  Created by Scott Haley on 5/9/16.
//  Copyright © 2016 Hallpass.io. All rights reserved.
//

import UIKit

class PinPad: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        button.backgroundColor = UIColor.redColor()
        
        self.addSubview(button)
    }

}
