//
//  ActionViewController.swift
//  Hallpass.io Ext
//
//  Created by Scott Haley on 5/20/16.
//  Copyright © 2016 Hallpass.io. All rights reserved.
//

import UIKit
import MobileCoreServices
import SwiftyJSON

class ActionViewController: UIViewController {

    @IBOutlet weak var SiteLabel: UILabel!
    @IBOutlet weak var LoginButton: UIButton!
    @IBOutlet weak var NoFormLabel: UILabel!
    
    var URL: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        API.sharedInstance.connect()
        
        
        let item: NSExtensionItem = extensionContext!.inputItems[0] as! NSExtensionItem
        let itemProvider: NSItemProvider = item.attachments?.first as! NSItemProvider
        
        if itemProvider.hasItemConformingToTypeIdentifier(kUTTypePropertyList as String) {
            itemProvider.loadItemForTypeIdentifier(kUTTypePropertyList as String, options: nil, completionHandler: { (item, error) -> Void in
                let dictionary = item as! NSDictionary
                NSOperationQueue.mainQueue().addOperationWithBlock {
                    let results = dictionary[NSExtensionJavaScriptPreprocessingResultsKey] as! NSDictionary
                    self.URL = results["URL"] as! String
                    let formExists = results["formExists"] as! Bool
                    
                    self.SiteLabel.text = self.URL
                    if !formExists {
                        self.LoginButton.hidden = true
                    } else {
                        self.NoFormLabel.hidden = true
                    }
                }
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func login() {
        let reasonID = API.sharedInstance.randomAlphanumericString(10)
        
        let defaults = NSUserDefaults.init(suiteName: "group.hallpass.io")
        let suid = defaults?.valueForKey("suid") as! String
        
        let requestData: [String: String] = ["suid": suid, "site": self.URL, "reasonID": reasonID]
        print(requestData)
        
        API.sharedInstance.emit("getcred", data: requestData)
        
        API.sharedInstance.on("reasons") {data in
            let json = JSON(data[0])
            for i in 0...(json.count - 1) {
                if json[i]["id"].string == reasonID {
                    // TODO: Show accounts
                    print(json[i])
                    
                    let description = json[i]["description"].string
                    let account = json[i]["accounts"][0].string
                    
                    API.sharedInstance.requestAuth(self, title: self.URL, description: description!, account: account!) { (success) in
                        
                    }
                    break
                }
            }
        }
        
        API.sharedInstance.on("getcred") {data in
            let json = JSON(data[0])
            let account = json["account"].string
            let pass = json["pass"].string
            
            let item = NSDictionary(object: NSDictionary(objects: [account!, pass!], forKeys: ["account", "pass"]),
                                    forKey: "NSExtensionJavaScriptFinalizeArgumentKey")
            
            let itemProvider = NSItemProvider(item: item, typeIdentifier: kUTTypePropertyList as String)
            let extensionItem = NSExtensionItem()
            extensionItem.attachments = [itemProvider]
            
            self.extensionContext?.completeRequestReturningItems([extensionItem], completionHandler: nil)
        }
    }
    
    @IBAction func done() {
        // Return any edited content to the host app.
        // This template doesn't do anything, so we just echo the passed in items.
        self.extensionContext!.completeRequestReturningItems(nil, completionHandler: nil)
    }

}
