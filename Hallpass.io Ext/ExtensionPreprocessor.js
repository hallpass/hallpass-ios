var HallpassExtensionClass = function() {};

HallpassExtensionClass.prototype = {
    formExists: function() {
        var passwordInput = document.querySelector('input[type="password"]');
        if (passwordInput) {
            return true;
        }
        return false;
    },
    
    getUsernameInput: function(formParent, pwInput) {
        var inputs = formParent.querySelectorAll('input')
        var index = Array.prototype.indexOf.call(inputs, pwInput);
        for (index; index; --index) {
            var type = inputs[index].getAttribute('type');
            if (type === 'text' || type === 'username' || type === 'email') {
                break;
            }
        }
        return inputs[index];
    },
    
    getParentForm(e) {
        do {
            e = e.parentNode;
        } while (e.tagName !== 'FORM');
        return e;
    },
    
    run: function(arguments) {
        var hostname = window.location.hostname;
        if (hostname.substring(0, 2) == "m.")
            hostname = "www." + hostname.substring(2)
        arguments.completionFunction({
                "URL": hostname,
                "formExists": this.formExists()
        });
    },
    
    finalize: function(arguments) {
        if (arguments["account"] == undefined || arguments["pass"] == undefined)
            return
        var pwInput = document.querySelector('input[type="password"]');
        if (!pwInput) {
            return;
        }
        
        var formParent = this.getParentForm(pwInput);
        var unInput = this.getUsernameInput(formParent, pwInput);
        
        unInput.value = arguments["account"];
        pwInput.value = arguments["pass"];
        formParent.submit();
    }
};

var ExtensionPreprocessingJS = new HallpassExtensionClass;